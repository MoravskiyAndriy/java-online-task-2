package com.moravskiyandriy;
import java.util.*;
import java.util.Random;

public class mytask2 {
    public static int generateRandom(int start, int end, ArrayList<Integer> exclude) {
        Random rand = new Random();
        int range = end - start +1 - exclude.size();
        int random = rand.nextInt(range) + 1;

        for(int i = 0; i < exclude.size(); i++) {
            if(!exclude.contains(random)) {
                return random;
            }
            random++;
        }

        return random;
    }

    public static void main(String[] args) {
        int n = 100;
        double probability=1;
        for(int i=1;i<n-3;i++){
            probability=probability*((n-3.-i)/(n-3.));
        }
        double mid=0;
        System.out.println("probability for all "+n+"("+(n-2)+") people to hear the rumor= "+probability);
        for(int z=0;z<1000;z++){
            ArrayList<Integer> arr=new ArrayList<Integer>();
            ArrayList<Integer> told=new ArrayList<Integer>();

            for(int i=0;i<n;i++){
                arr.add(i);
            }
            Random rand = new Random();
            int said=0;
            int Alise=n - 1;
            rand = new Random();
            int person=rand.nextInt(n-2) + 1;
            told.add(said);
            told.add(Alise);
            told.add(person);
            //System.out.println(said+" "+Alise+" "+person);

            for(int j=1;j<arr.size()-2;j++) {
                ArrayList<Integer> exclude=new ArrayList<Integer>();
                exclude.add(Alise);
                exclude.add(said);
                exclude.add(person);
                //System.out.println(exclude);
                int randomInt = generateRandom(0,n,exclude);
                //System.out.println(randomInt);
                //System.out.println();
                if(told.contains(randomInt)&&told.size()<n-3){
                    told.add(randomInt);
                    said=person;
                    person=randomInt;
                    //System.out.println(said+" => "+person);
                    //System.out.println("Not all");
                    break;
                }

                told.add(randomInt);
                said=person;
                person=randomInt;
                //System.out.println(said+" => "+person);
//            for(int in:told){
//                System.out.print(in);
//            }
                //System.out.println();
                //System.out.println(told.size()/(n-3));
            }
            //System.out.println(told);
            //System.out.println(told.size()/((double)n));
            mid=mid+told.size()/((double)n);
        }
        System.out.println("Medium percentage of people who actually heard the rumor after 1000 experiments: "+ mid/1000);

    }
}
